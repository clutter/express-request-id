# Express Request ID
Request ID Tracing middleware for ExpressJS

## Installation

```bash
$ npm install @parthar/express-request-id --save
```

## Usage

```js
// setup expressjs middleware
var expressReqId = require("@parthar/express-request-id");
var express = require("express");
var app = express();
// use req-id when present on req[headerName]; otherwise, generate a uuid.v4()
// make req-id available on req[key], and add it to response headers
app.use(expressReqId({
    "headerName": "x-request-id",
    "key": "_xReqId"
}));

```
