"use strict";

const uuid = require("uuid-random");

const defaults = {
    "headerName": "x-request-id",
    "key": "_xReqId"
};

function expressReqId(options) {
    const opts = Object.assign({}, defaults, options);

    return function expressReqIdMiddleware(req, res, next) {
        let id = req.headers[opts.headerName] || uuid();

        req[opts.key] = id;
        res.setHeader(opts.headerName, id);
        next();
    };
}

module.exports = expressReqId;
