/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const assert = require("assert");
const uuid = require("uuid-random");
const expressReqId = require("../");

before(function (done) {
    done();
});

describe("express-request-id", function () {
    it("should export express middleware", function (done) {
        assert.strictEqual(typeof expressReqId(), "function");
        assert.strictEqual(expressReqId().length, 3);
        done();
    });
    it("should use existing req-id when present", function (done) {
        let reqId = "some-string";
        let req = {
            "headers": {
                "x-request-id": reqId
            }
        };

        expressReqId()(req, {
            "setHeader": function (hdr, id) {
                assert.strictEqual(hdr, "x-request-id");
                assert.strictEqual(id, reqId);
            }
        }, function () {
            assert.strictEqual(req._xReqId, reqId);
            done();
        });
    });
    it("should generate req-id when not present", function (done) {
        let reqId;
        let req = {
            "headers": {}
        };

        expressReqId()(req, {
            "setHeader": function (hdr, id) {
                assert.strictEqual(hdr, "x-request-id");
                assert.ok(uuid.test(id));
                reqId = id;
            }
        }, function () {
            assert.strictEqual(req._xReqId, reqId);
            done();
        });
    });
});

after(function (done) {
    done();
});
